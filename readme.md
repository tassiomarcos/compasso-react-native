![N|Solid](screenshots/app_logo.png)

# Desafio React Naitve - Compasso

### Screenshots

#### dark

<table>
  <tr>
    <td>Home</td>
     <td>Modal</td>
     <td>Settings</td>
  </tr>
  <tr>
    <td><img src="screenshots/screenshots/dark1.png" width=270 height=480></td>
    <td><img src="screenshots/screenshots/dark2.png" width=270 height=480></td>
    <td><img src="screenshots/screenshots/dark3.png" width=270 height=480></td>
  </tr>
 </table>

#### light

![N!Solid](screenshots/splash.png)![N!Solid](screenshots/light1.png)![N!Solid](screenshots/light2.png)![N!Solid](screenshots/light3.png)

### Run and Build !?!

- Run DEBUG

  - android:

    - in terminal run yarn, then run 'yarn android'

  - ios:

    - in project folder run 'yarn', then run 'yarn ios'

  * Run RELEASE

    - android:
      - apk: run at the terminal in the root folder the command: 'yarn build:apk'
      - bundle release: run at the terminal in the root folder the command: 'yarn build:bundle'
    - ios:
      - not available

* Make sure you have installed the project packages. Run in terminal 'yarn'.
